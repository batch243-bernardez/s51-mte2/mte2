let collection = [];
// Write the queue functions below.


function print() {
    // It will show the array

    return collection;
}


function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method

    let i = collection.length

    collection[i] = element
    
    return collection
}


function dequeue() {
    // In here you are going to remove the first element in the array

    const removeElement = [];

    if (collection.length !== 0) {
        for (let i = 1; i < collection.length; i++) {
            removeElement[i - 1] = collection[i]
        }
    }
    collection = removeElement

    return collection;
}


function front() {
    // In here, you are going to capture the first element

    let first = collection[0];
        return first;
}
    

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements   
    
    let lengthCollection = 0;
    while (collection[lengthCollection])
        lengthCollection++

    return lengthCollection;
}
    

function isEmpty() {
    //it will check whether the array is empty or not

    if (collection[0]) {
        return false;
    } else {
        return true;
    }

}


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};